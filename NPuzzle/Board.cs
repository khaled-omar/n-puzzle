﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle
{
    /*
     * Class Board
     * Writte by : khaled 
    */
    class Board: IEqualityComparer<Board>
    {
        
        //------[variables] ---------//
        private int[,] Arr;
        private int SIZE;
      

        //------[Methods] ---------//

        /*
         * CONSTRUCTOR
         * Writte by  khaled 
         */
        public Board(int N,int [,]inputArr)
        {
            //initialize SIZE
            SIZE = N;
            Arr = inputArr;
 
            
        }

     //check if Solvable
        public bool isSolvable()
        {
            int x = 0,row0=0;
            int []oneDimension = new int[SIZE*SIZE];
            int inversions = 0;
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < SIZE; j++)
                {
                    if(Arr[i, j]==0)
                    {
                        row0 = i ;
                    }
                    oneDimension[x] = Arr[i, j];
                    x++;

                }
            }
            for (int i = 0; i < SIZE * SIZE -1 ; i++)
            {
                if (oneDimension[i] == 0 )
                {
                    continue;
                }
                for (int j =i+1; j < SIZE * SIZE ; j++)
                {
                    if (oneDimension[j] == 0)
                    {
                        continue;
                        
                    }
                    if (oneDimension[i] > oneDimension[j] )
                    {
                        inversions++;
                    }
                }
            }
            if (SIZE % 2 != 0)
                return (inversions % 2 == 0);
            else
            {
                return ((row0 + inversions) % 2 != 0);
            }
        }

        /* Esraa Ahmed */
        public int hamming()
        {
            int num = 0;
            int x = 1;

            //int[,] Arr = A;
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < SIZE; j++)
                {
                    if (i == SIZE - 1 && j == SIZE - 1)
                    {
                        break;
                    }

                    if (Arr[i, j] != x )
                    {
                        num++;
                    }

                    x++;
                 
                }
            }
            return num; 
        }
        
        /* Donia Tarek */
        public void print()
        {
            // print arr content
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < SIZE; j++)
                {
                    Console.Write(Arr.GetValue(i, j) + "  ");
                   
                }
                Console.WriteLine();
            }

        }

        /* get the Board */
        public int[,] getArr()
        {
            return Arr;
        }
        public int getSize()
        {
            return SIZE;
        }

        /* Donia tarek */
        public bool Equals(Board Y)
        {
            return Array.Equals(this.Arr, Y.Arr);
  
        }
        public  bool Equals(object x, object y)
        {
            Board B1 = x as Board;
            Board B2 = y as Board;
            for (int i = 0; i < SIZE; i++)
            {
                for (int j = 0; j < SIZE; j++)
                {
                    if (B1.Arr[i, j] != B2.Arr[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
          
        }


        public  int Compare(Board x, Board y)
        {
                for (int i = 0; i < SIZE; i++)
                {
                    for (int j = 0; j < SIZE; j++)
                    {
                        if (x.Arr[i, j] > y.Arr[i, j])
                        {
                            return 1;
                           
                        }
                        else if (x.Arr[i, j] < y.Arr[i, j])
                        {
                            return -1;
                           
                        }
                    }
                }
                return 0;
        }



        public bool Equals(Board x, Board y)
        {
           return Array.Equals(x.Arr, y.Arr);
        }




        public int GetHashCode(Board obj)
        {
            
            return 0; 
        }
        public int ManhattanDistance()
        {
            int manhattanDistanceSum = 0;
            for (int x = 0; x < SIZE; x++) 
                for (int y = 0; y < SIZE; y++)
                { 
                    int value = Arr[x,y]; 
                    if (value != 0)
                    { 
                        int targetX = (value - 1) / SIZE; 
                        int targetY = (value - 1) % SIZE;
                        int dx = x - targetX; 
                        int dy = y - targetY; 
                        manhattanDistanceSum += Math.Abs(dx) + Math.Abs(dy);
                    }
                }
           return manhattanDistanceSum;
        }
    }
}
