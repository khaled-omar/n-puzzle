﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPuzzle
{
    class Moves
    {
       private int [,] arr;
       private int size, i, j;
       private int[,] copied;
       private int counter;
       private LinkedList<Node> possibles;

        public Moves(int[,] ar, int SIZE)
        {
            possibles = new LinkedList<Node>();
            counter = 0;
            arr = ar;
            this.size = SIZE;
            copy(); 
            for (int ii = 0; ii < size; ii++)
            {
                for (int jj = 0; jj < size; jj++)
                {
                    if (arr[ii, jj] == 0)
                    {
                        i = ii;
                        j = jj;
                        break;
                    }
                }
             }
        }

        public LinkedList<Node> possibleMoves()
        {
        
            if (up())
            {

                possibles.AddLast(new Node(new Board(size, copied), size));
            }


            if (left())
            {
                possibles.AddLast(new Node(new Board(size, copied), size));

            }

            if (down())
            {

                possibles.AddLast(new Node(new Board(size, copied), size));
            }


            if (right())
            {
                possibles.AddLast(new Node(new Board(size, copied), size));
            }

           return possibles;

           
        }
        public bool up()
        {
            copy();
            if (i + 1 <= size - 1)
            {

                swap(ref copied[i + 1, j], ref copied[i, j]);
                counter++;
                return true;
            }
            return false;
        }
        public bool down()
        {
            copy();
            if (i - 1 >= 0)
            {
                // Index out of range
                counter++;
                swap(ref copied[i - 1, j], ref copied[i, j]);
                return true;
            }
            return false;
        }
        public bool left()
        {
             copy();
            if (j + 1 <= size -1)
            {
                counter++;
                swap(ref copied[i, j + 1], ref copied[i, j]);
                return true;
            }
            return false;
        }
        public bool right()
        {
            copy();
            if (j - 1 >=0)
            {

                swap(ref copied[i, j - 1], ref copied[i, j]);
                counter++;
                return true;
            }
            return false;
        }
        public void copy()
        {
            copied = new int [size,size];
            for (int ii = 0; ii < size; ii++)
            {
                for (int jj = 0; jj < size; jj++)
                {
                    copied[ii, jj] = arr[ii, jj];
                }
            }
          

        }
        public void swap(ref int first, ref int second)
        {
            int temp = first;
            first = second;
            second = temp;

        }
        public int getCounter()
        {
            return counter;
        }
    }
}
