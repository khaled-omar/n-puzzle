﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Priority_Queue;
namespace NPuzzle
{

    /*
     * Class Node
     * Writte by : khaled 
    */
    class Node : PriorityQueueNode ,IComparer<Node>,IComparable<Node>,IEqualityComparer<Node>
    {
        
        /*
         * variables 
         */
        //static int Num = 0;
        //private int Hash;
        private Board Current;
        private int movesSoFar;
        private Node previous;
        private int fScore;
        public Node(Board b, int size,int moves)
        {
            Current = b;
            movesSoFar = moves;
            //Num ++;
            //Hash = Num;
        }
        public Node(Board b, int size)
        {
            Current = b;
            //Num++;
            //Hash = Num;
           
        }
        public int getFscore()
        {
            fScore = movesSoFar + Current.hamming();
            return fScore;
        }
        public LinkedList<Node> getChildren()
        {
            Moves movements = new Moves(Current.getArr(), Current.getSize());

            LinkedList <Node> MoveMents = movements.possibleMoves();
            foreach (Node N in MoveMents)
            {
                N.previous = this;
                N.movesSoFar = this.movesSoFar + 1;
            }
            return MoveMents;
        }
        public void print()
        {
            // print arr content
            for (int i = 0; i <Current.getSize() ; i++)
            {
                for (int j = 0; j < Current.getSize(); j++)
                {
                    Console.Write(Current.getArr().GetValue(i, j) + "\t");

                }
                
                Console.WriteLine("\n");
                

            }
            Console.WriteLine("==> Priority =  " + this.getFscore());
            
            
            
        }
        
        public void  Childs()
        {
                Console.WriteLine("\n#########[[[Childrens]]]##########\n");

                foreach(Node N in getChildren() )
                {
                    N.print();
                    Console.WriteLine();
                }
                
         }
        public Board getCurrent()
        {
            return Current;
        }
        public Node getPrevious()
        {
            return previous;
        }
        public int getMovesSofFar()
        {
            return movesSoFar;
        }
        public void SetPrevious(Node b)
        {
             previous = b;
        }


        public int Compare(Node x, Node y)
        {

            return Current.Compare(x.Current, y.Current);
        }

        public int CompareTo(Node obj)
        {
            return Compare(this, obj);
        }


        public  bool Equals(Node x, Node y)
        {
            return this.Equals(y);
        }
        public  bool Equals(Node Y)
        {
            return (Current.Equals(Y.Current));
        }



        public int GetHashCode(Node obj)
        {
            return 0;
        }
    }
}
