﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace NPuzzle
{
    struct pairedArrSize
    {
        public int[,] arr;
        public int size;
    }
    /* n-puzzle-solver.appspot.com */
    class Program
    {
        /* Esraa Ahmed */
        static pairedArrSize readFile(string filePath)
        {
            pairedArrSize p;
            // Read the initial array from the input file into arr 
            FileStream FS = new FileStream(filePath, FileMode.Open);
            StreamReader SR = new StreamReader(FS);
            
            int N = int.Parse(SR.ReadLine());
            p.size = N;
            int[,] arr1 = new int[N, N];

            String line = null;

            int row = 0;
            while ((line = SR.ReadLine()) != null)
            {

                String[] arr2 = line.Split(' ');

                for (int i = 0; i < arr2.Length; i++)
                {
                    arr1[row, i] = int.Parse(arr2[i]);

                }
                row++;
            }

            SR.Close();
            p.arr=arr1;
            return p;
        } 
        static void Main(string[] args)
        {
            int TimeBefore = Environment.TickCount;
            pairedArrSize p= readFile("Npuzzle.txt");
            Board B = new Board(p.size, p.arr);
            if (B.isSolvable())
            {
                
                Node initialNode = new Node(B, p.size, 0);
                Console.WriteLine("solvable Board !\n");
                
                Solver sol = new Solver();
                sol.aStar(initialNode);
              

                int TimeAfter = Environment.TickCount;
                Console.WriteLine("Time in Miliseconds = " + (TimeAfter - TimeBefore) + " ms");
            }
            else
                Console.WriteLine("Unsolvable Board !\n");

           
        }
    }
}
