﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Priority_Queue;
namespace NPuzzle
{
    /*
     * Class Solver
     * Writte by : khaled 
    */
    class Solver
    {
        HeapPriorityQueue<Node> openSet;
        private LinkedList<Node> SolutionPath;
        private Node CurrentNode;
        //private Dictionary<int,Node> CloseTable;
        private SortedSet<Node> Close;
        //private SortedSet<Node> Open;
          
        /*
         * Constructor 
         */
        public Solver()
        {

            openSet = new HeapPriorityQueue<Node>(50);
            // A* Algorithm Here
            SolutionPath = new LinkedList<Node>();
            
            //Open = new SortedSet<Node> ();       // The set of tentative nodes to be evaluated

            //CloseTable = new Dictionary<int, Node>();
            Close = new SortedSet<Node>();

         
        }
        public void aStar(Node initialNode)
        {
            initialNode.SetPrevious(null);
           
            openSet.Enqueue(initialNode, initialNode.getFscore());
            //Open.Add(initialNode);

            while (openSet.Count > 0) //  Open.Count > 0
            {

                //CurrentNode = FindMaxPriorit(); //open[open.Keys.Min()]; //  // here i need to know the max priority Node
                CurrentNode = openSet.Dequeue();
                /// Check if th ecurrent state is the goal
                if (CurrentNode.getCurrent().ManhattanDistance() == 0)
                {
                    constructPath(CurrentNode);
                    break;
                }
                //Open.Remove(CurrentNode);
                //openSet.Remove(CurrentNode);
                Close.Add(CurrentNode);
                //CloseTable.Add(CurrentNode.GetHashCode(initialNode), initialNode);
                LinkedList<Node> Children = CurrentNode.getChildren();

                //Console.WriteLine("\n#Visited = " + Close.Count);

                foreach (Node N in Children)
                {
                    // CloseTable.ContainsKey(N.GetHashCode())
                    if (Close.Contains(N))
                    {
                        continue;
                    }

                    if (!openSet.Contains(N))
                    {
                        openSet.Enqueue(N, N.getFscore());


                    }


                }

            }
            this.Solution();
            Console.WriteLine("Number of moves  = " + this.numOfMoves());
            
        }
        private void constructPath(Node Current)
        {
            
            // Construct the riht path
            while (Current!= null)
            {
                SolutionPath.AddFirst(Current);
                Current = Current.getPrevious();
            }
        }
        public void Solution()
        {

            // print the queue
            Console.WriteLine("\nSolution Path\n\n ");
            foreach (Node N in SolutionPath)
            {
               
                N.print();
                Console.WriteLine("\n");
            }
        }
        public int numOfMoves()
        {
            return SolutionPath.Count - 1;
        }
        //private Node FindMaxPriorit()
        //{
        //    Node min = Open.ElementAt(0);

        //    foreach (Node N in Open)
        //    {
        //        if (N.getFscore() < min.getFscore())
        //            min = N;
        //    }
        //    return min;
           
        //}

        
    }
}
